import { StyleSheet, Text, View, TextInput, Image, TouchableOpacity } from 'react-native';
import React from 'react';
import { COLORS } from '../constants';

const InputField = ({ container, label, postFixImage, postFixContainer, postFixImageContainer, postPress, setText, defaultvalue, secureTextEntry, keyboardType, type , typeText , typeTextStyle }) => {
    return (
        <View style={{
            ...container,
        }}>
            <Text style={{
                letterSpacing: 1,
                color: COLORS.white,
                fontWeight: '300',
            }}>{label}</Text>

            <View style={{
                backgroundColor: COLORS.darkGray,
                height: 45,
                width: '100%',
                borderRadius: 3,
                marginTop: 6,
                flexDirection: 'row',
                justifyContent: 'space-between',

            }}>
                <View>
                    <TextInput
                        keyboardType={keyboardType}
                        secureTextEntry={secureTextEntry}
                        defaultValue={defaultvalue}
                        onChangeText={(text) => { setText(text) }}
                        selectionColor={COLORS.primary}
                        style={{
                            color: COLORS.white,
                            paddingHorizontal: 10
                        }}
                    />
                </View>


                <View style={{
                    alignItems: 'center',
                    justifyContent: 'center',
                    ...postFixContainer
                }}>
                    <TouchableOpacity onPress={postPress}>

                        <Image source={postFixImage} style={{ ...postFixImageContainer }} />


                        {type == 'text' &&
                            <Text style={{...typeTextStyle}}>{typeText}</Text>
                        }
                    </TouchableOpacity>


                </View>

            </View>
        </View>
    );
};

export default InputField;

const styles = StyleSheet.create({});
