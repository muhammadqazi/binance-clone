import { StyleSheet, Text, View, TouchableOpacity } from 'react-native';
import React from 'react';
import { COLORS } from '../constants';

const TextButton = ({ label, onPress , containerStyle , labelStyle }) => {
    return (
        <TouchableOpacity style={{
            backgroundColor: COLORS.dark_primary,
            height: 40,
            borderRadius: 3,
            alignItems: 'center',
            justifyContent: 'center',
            ...containerStyle,
            ...styles.shadow
        }}
            onPress={onPress}
        >
            <Text style={{
                color: COLORS.black,
                ...labelStyle

            }}>{label}</Text>
        </TouchableOpacity>
    );
};

export default TextButton;

const styles = StyleSheet.create({
    shadow: {
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 4
        },
        shadowOpacity: 0.30,
        shadowRadius: 4.65,

        elevation: 8
    }
})
