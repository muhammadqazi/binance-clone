import { StyleSheet, Text, View, TouchableOpacity } from 'react-native'
import React from 'react'
import { COLORS, icons } from '../../../constants'
import { InputField, TextButton } from '../../../components'

const New_device = () => {


    function renderHeader() {
        return (
            <View style={{
                marginHorizontal: 20,
                marginTop: 50
            }}>
                <Text style={{
                    fontWeight: 'bold',
                    color: COLORS.white,
                    letterSpacing: 1,
                    fontSize: 22,

                }}>Confirm New Device Login</Text>
            </View>
        )
    }

    function renderForm() {
        return (
            <View>
                <View style={{
                    marginHorizontal: 20,
                    marginTop: 30,
                }}>

                    <InputField
                        label="E-mail verification code"
                        type={'text'}
                        typeText={'Get Code'}
                        typeTextStyle={{
                            color: COLORS.dark_primary,
                            fontWeight: '600',
                            marginRight: 10,
                        }}
                        postPress={() => { console.log('pressed') }}
                    />

                </View>

                <View style={{
                    marginHorizontal: 20,
                    marginTop: 20,
                }}>
                    <Text style={{
                        color: COLORS.darkGray2,
                        letterSpacing: 0.5,
                        fontSize: 12,
                        marginTop: 5
                    }}>Enter the 6-digit code sent to pri***@gmail.com</Text>

                </View>

                <View style={{
                    marginHorizontal: 20,
                    marginTop: 40,
                }}>

                    <InputField
                        label="Authenticatior code"
                        type={'text'}
                    />

                </View>

                <TouchableOpacity style={{
                    marginHorizontal: 20,
                    marginTop: 20,
                }}>
                    <Text style={{
                        color: COLORS.primary,
                    }}>

                        Security verification unavailable?
                    </Text>
                </TouchableOpacity>

                <View style={{
                    marginHorizontal: 20,
                    marginTop: 20,
                }}>
                    <TextButton 
                        label={'Submit'}
                        onPress={() => console.log('New Device')}
                    />
                </View>
            </View>
        )
    }


    return (

        <View style={{
            flex: 1,
            backgroundColor: COLORS.primary_back
        }}>

            {/* Header */}
            {renderHeader()}

            {/* Form */}
            {renderForm()}




        </View>

    )
}

export default New_device

const styles = StyleSheet.create({})