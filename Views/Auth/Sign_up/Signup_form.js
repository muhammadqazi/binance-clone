import { StyleSheet, Text, View, TouchableOpacity, Image } from 'react-native';
import React, { useState } from 'react';
import { COLORS, icons } from '../../../constants';
import { InputField, TextButton } from '../../../components';

const Signup_form = () => {


    const [Tab, setTab] = useState(true)
    const [email, setEmail] = useState("")
    const [password, setPassword] = useState("")
    const [hidePassword, setHidePassword] = useState(true)
    const [phone, setPhone] = useState("")
    const [refHide, setRefHide] = useState(false)
    const [referal, setReferal] = useState("")
    const [emailUp, setEmailUp] = useState(false)
    const [terms, setTerms] = useState(true)

    function toggleHidePass() {
        setHidePassword(!hidePassword)
    }

    function toggleRef() {
        setRefHide(!refHide)
    }

    function toggleTerms() {
        setTerms(!terms)
    }
    function toggleEmailUp() {
        setEmailUp(!emailUp)
    }

    function renderHeader() {
        return (
            <View style={{
                marginHorizontal: 20,
                marginTop: 50
            }}>
                <Text style={{
                    fontWeight: 'bold',
                    color: COLORS.white,
                    letterSpacing: 1,
                    fontSize: 22,
                }}>Create Binance Account</Text>
            </View>
        )
    }

    function renderTabs() {
        return (
            <View style={{
                flexDirection: 'row',
                marginTop: 20,
            }}>
                <TouchableOpacity style={{
                    height: 30,
                    width: 60,
                    borderRadius: 3,
                    backgroundColor: Tab ? COLORS.darkGray : COLORS.primary_back,
                    marginLeft: 20,
                    marginRight: 8,
                    justifyContent: 'center',
                    alignItems: 'center',
                }}
                    onPress={() => { setTab(true) }}
                >

                    <Text style={{
                        color: Tab ? COLORS.white : COLORS.darkGray,
                        fontWeight: '400',
                    }}>Email</Text>


                </TouchableOpacity>
                <TouchableOpacity style={{
                    height: 30,
                    width: 110,
                    borderRadius: 3,
                    backgroundColor: Tab ? COLORS.primary_back : COLORS.darkGray,
                    justifyContent: 'center',
                    alignItems: 'center',
                }}
                    onPress={() => setTab(false)}
                >

                    <Text style={{
                        color: Tab ? COLORS.darkGray : COLORS.white,
                        fontWeight: '400',
                    }}>Phone Number</Text>


                </TouchableOpacity>
            </View>
        )
    }

    function renderForm() {
        return (
            <View>
                <View style={{
                    marginHorizontal: 20,
                    marginTop: 30,
                }}>
                    <InputField
                        keyboardType={Tab ? "email-address" : "phone-pad"}
                        defaultvalue={Tab ? email : phone}
                        secureTextEntry={false}
                        label={Tab ? 'Email' : 'Phone Number'}
                        setText={Tab ? setEmail : setPhone}
                        postFixImage={icons.cross}
                        postFixImageContainer={{
                            height: 23,
                            width: 23,
                            tintColor: COLORS.gray,
                        }}
                        postFixContainer={{
                            marginRight: 10,
                        }}
                        postPress={() => { Tab ? setEmail("") : setPhone("") }}
                    />
                </View>

                <View style={{
                    marginHorizontal: 20,
                    marginTop: 20,
                }}>
                    <InputField
                        label={'Password'}
                        secureTextEntry={hidePassword}
                        setText={setPassword}
                        postFixImage={hidePassword ? icons.eye_off : icons.eye}
                        postFixImageContainer={{
                            height: 25,
                            width: 25,
                            tintColor: COLORS.gray,
                        }}
                        postFixContainer={{
                            marginRight: 10,
                        }}
                        postPress={() => { toggleHidePass() }}
                    />
                </View>


                <TouchableOpacity onPress={toggleRef}>
                    <View style={{
                        marginHorizontal: 20,
                        marginTop: 20,
                        flexDirection: 'row',
                        alignItems: 'center',
                    }}>

                        <Text style={{
                            letterSpacing: 1,
                            color: COLORS.white,
                            fontWeight: '300',
                        }}>Referral ID (Optional)</Text>

                        <Image style={{
                            height: 15,
                            width: 15,
                            tintColor: COLORS.darkGray,
                            marginLeft: 5,
                            transform: refHide ? [{ rotate: '180deg' }] : [{ rotate: '0deg' }]

                        }} source={icons.up} />

                    </View>
                </TouchableOpacity>

                {!refHide &&
                    <View style={{
                        marginHorizontal: 20,
                    }}>
                        <InputField
                            secureTextEntry={false}
                            setText={setReferal}

                        />
                    </View>
                }

            </View>
        )
    }

    function renderTerms() {
        return (
            <View style={{
                marginHorizontal: 20,
                marginTop: 20,
            }}>
                <View style={{
                    flexDirection: 'row',
                    alignItems: 'center',
                }}>
                    <TouchableOpacity onPress={toggleEmailUp}>
                        <Image source={icons.tick} style={{
                            height: 15,
                            width: 15,
                            tintColor: emailUp ? COLORS.primary : COLORS.darkGray,
                        }} />
                    </TouchableOpacity>

                    <Text style={{
                        marginLeft: 10,
                        letterSpacing: 1,
                        color: COLORS.white,
                        fontSize: 12
                    }}>I agreed to receive email updates from Binance</Text>
                </View>

                <View style={{
                    marginTop: 10,
                    flexDirection: 'row',
                    alignItems: 'center',
                }}>
                    <TouchableOpacity onPress={toggleTerms}>
                        <Image source={icons.tick} style={{
                            height: 15,
                            width: 15,
                            tintColor: terms ? COLORS.primary : COLORS.darkGray,
                        }} />
                    </TouchableOpacity>

                    <Text style={{
                        marginLeft: 10,
                        letterSpacing: 1,
                        color: COLORS.white,
                        fontSize: 12
                    }}>I have read and agreed to Binance's
                        <Text style={{
                            color: COLORS.brown,
                        }}> Terms of Service</Text>
                    </Text>
                </View>

            </View>
        )
    }

    function renderButton() {
        return (
            <View>
                <View style={{
                    marginHorizontal: 20,
                    marginTop: 30
                }}>
                    <TextButton
                        onPress={() => console.log('Sign In')}
                        label={'Create Account'}
                    />
                </View>

                <View style={{
                    marginHorizontal: 20,
                    marginTop: 30,
                    flexDirection: 'row',
                }}>
                    <Text style={{
                        color: COLORS.darkGray2
                    }}>Already Registered? </Text>

                    <TouchableOpacity>
                        <Text style={{
                            color: COLORS.brown,
                        }}>Login</Text>
                    </TouchableOpacity>


                </View>
            </View>
        )
    }



    return (

        <View style={{
            flex: 1,
            backgroundColor: COLORS.primary_back
        }}>

            {/* Header */}
            {renderHeader()}


            {/* Tabs */}
            {renderTabs()}


            {/* Form */}
            {renderForm()}

            {/* Terms */}
            {renderTerms()}


            {/* Button */}
            {renderButton()}

        </View>



    );
};

export default Signup_form;

const styles = StyleSheet.create({});
