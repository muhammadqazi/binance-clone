import { StyleSheet, Text, View, Image, TouchableOpacity } from 'react-native';
import React from 'react';
import { COLORS, icons } from '../../../constants';
import { TextButton } from '../../../components';

const Sign_up = () => {

    function renderHeader() {
        return (
            <View style={{
                marginHorizontal: 20,
                marginTop: 50
            }}>
                <Text style={{
                    fontWeight: 'bold',
                    color: COLORS.white,
                    letterSpacing: 1,
                    fontSize: 22,
                }}>Where do you live?</Text>
                <Text style={{
                    fontWeight: '300',
                    color: COLORS.darkGray,
                    letterSpacing: 0.5,
                    fontSize: 14,
                    marginTop: 15
                }}>Before we start, please enter your current location of residence.</Text>

            </View>
        )
    }

    function renderCountries() {
        return (
            <View style={{
                marginHorizontal: 20,
                marginTop: 25
            }}>
                <Text style={{
                    fontWeight: '300',
                    color: COLORS.gray,
                    letterSpacing: 0.5,
                    fontSize: 14,
                }}>Country/Area of Residence</Text>

                <View style={{
                    marginTop: 5,
                    height: 40,
                    backgroundColor: COLORS.darkGray,
                    borderRadius: 3,
                    flexDirection: 'row',
                    justifyContent: 'space-between',
                    alignItems: 'center'
                }}>
                    <Text style={{
                        fontWeight: '300',
                        color: COLORS.white,
                        letterSpacing: 0.5,
                        fontSize: 14,
                        marginLeft: 10
                    }}>Qatar (قطر)</Text>

                    <TouchableOpacity>
                        <Image style={{
                            height: 20,
                            width: 20,
                            tintColor: COLORS.gray2,
                            marginRight: 10
                        }} source={icons.forward} />
                    </TouchableOpacity>
                </View>
            </View>
        )
    }

    function renderFooter() {
        return (
            <View style={{
                marginHorizontal: 20,
            }}>
                <Text style={{
                    color: COLORS.darkGray2,
                    letterSpacing: 0.5,
                    fontSize: 12,
                    marginTop: 15
                }}>The registration process is subject to change based on the information you provide</Text>

                <View style={{
                    marginTop: 30
                }}>
                    <TextButton
                        onPress={() => console.log('Sign Up')}
                        label={'Confirm'}
                    />
                </View>
            </View>
        )
    }
    return (
        <View style={{
            flex: 1,
            backgroundColor: COLORS.primary_back
        }}>

            {/* Header */}
            {renderHeader()}

            {/* Countries */}
            {renderCountries()}


            {/* Footer */}
            {renderFooter()}

        </View>
    );
};

export default Sign_up;

const styles = StyleSheet.create({});
