import { StyleSheet, Text, View, TouchableOpacity, TextInput } from 'react-native';
import React, { useState } from 'react';
import { COLORS, icons } from '../../../constants';
import { InputField, TextButton } from '../../../components';

const Sign_In = () => {


    const [Tab, setTab] = useState(true)
    const [email, setEmail] = useState("")
    const [password, setPassword] = useState("")
    const [hidePassword, setHidePassword] = useState(true)
    const [phone, setPhone] = useState("")

    function toggleHidePass() {
        setHidePassword(!hidePassword)
    }

    function renderHeader() {
        return (
            <View style={{
                marginHorizontal: 20,
                marginTop: 50
            }}>
                <Text style={{
                    fontWeight: 'bold',
                    color: COLORS.white,
                    letterSpacing: 1,
                    fontSize: 22,
                    
                }}>Binance Account Login</Text>
            </View>
        )
    }

    function renderTabs() {
        return (
            <View style={{
                flexDirection: 'row',
                marginTop: 20,
            }}>
                <TouchableOpacity style={{
                    height: 30,
                    width: 60,
                    borderRadius: 3,
                    backgroundColor: Tab ? COLORS.darkGray : COLORS.primary_back,
                    marginLeft: 20,
                    marginRight: 8,
                    justifyContent: 'center',
                    alignItems: 'center',
                }}
                    onPress={() => { setTab(true) }}
                >

                    <Text style={{
                        color: Tab ? COLORS.white : COLORS.darkGray,
                        fontWeight: '400',
                    }}>Email</Text>


                </TouchableOpacity>
                <TouchableOpacity style={{
                    height: 30,
                    width: 110,
                    borderRadius: 3,
                    backgroundColor: Tab ? COLORS.primary_back : COLORS.darkGray,
                    justifyContent: 'center',
                    alignItems: 'center',
                }}
                    onPress={() => setTab(false)}
                >

                    <Text style={{
                        color: Tab ? COLORS.darkGray : COLORS.white,
                        fontWeight: '400',
                    }}>Phone Number</Text>


                </TouchableOpacity>
            </View>
        )
    }

    function renderForm() {
        return (
            <View>
                <View style={{
                    marginHorizontal: 20,
                    marginTop: 30,
                }}>
                    <InputField
                        keyboardType={Tab ? "email-address" : "phone-pad"}
                        defaultvalue={Tab ? email : phone}
                        secureTextEntry={false}
                        label={Tab ? 'Email' : 'Phone Number'}
                        setText={Tab ? setEmail : setPhone}
                        postFixImage={icons.cross}
                        postFixImageContainer={{
                            height: 23,
                            width: 23,
                            tintColor: COLORS.gray,
                        }}
                        postFixContainer={{
                            marginRight: 10,
                        }}
                        postPress={() => { Tab ? setEmail("") : setPhone("") }}
                    />
                </View>

                <View style={{
                    marginHorizontal: 20,
                    marginTop: 20,
                }}>
                    <InputField
                        label={'Password'}
                        secureTextEntry={hidePassword}
                        setText={setPassword}
                        postFixImage={hidePassword ? icons.eye_off : icons.eye}
                        postFixImageContainer={{
                            height: 25,
                            width: 25,
                            tintColor: COLORS.gray,
                        }}
                        postFixContainer={{
                            marginRight: 10,
                        }}
                        postPress={() => { toggleHidePass() }}
                    />
                </View>
            </View>
        )
    }

    function renderButtons() {
        return (
            <View>
                <View style={{
                    marginHorizontal: 20,
                    marginTop: 30
                }}>
                    <TextButton
                        onPress={() => console.log('Sign In')}
                        label={'Log In'}
                    />
                </View>

                <View style={{
                    marginHorizontal: 20,
                    marginTop: 30
                }}>
                    <TouchableOpacity>
                        <Text style={{
                            color: COLORS.brown,
                            fontWeight: '400',
                        }}>Forgot password?</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={{
                        marginTop: 20
                    }}>
                        <Text style={{
                            color: COLORS.brown,
                            fontWeight: '400',
                        }}>Register now</Text>
                    </TouchableOpacity>
                </View>
            </View>
        )
    }

    return (

        <View style={{
            flex: 1,
            backgroundColor: COLORS.primary_back
        }}>

            {/* Header */}
            {renderHeader()}


            {/* Tabs */}
            {renderTabs()}

            {/* Form */}
            {renderForm()}

            {/* Buttons */}
            {renderButtons()}

        </View>



    );
};

export default Sign_In;

const styles = StyleSheet.create({});
