import { StyleSheet, Text, View, TouchableOpacity, Image } from 'react-native'
import React from 'react'
import { COLORS, icons } from '../../../../constants'
import { TextButton } from '../../../../components'


const Portfolio = ({ icon, label, amount, symbol, symbolTextStyle }) => {
  return (
    <View style={{
      marginTop: 35,
      flexDirection: 'row',
      alignItems: 'center',
      justifyContent: 'space-between',
    }}>

      <View style={{
        flexDirection: 'row',
      }}>
        <Image
          source={icon}
          style={{
            height: 17,
            width: 17,
            tintColor: COLORS.gray2,
          }}
        />


        <Text style={{
          color: COLORS.gray2,
          fontWeight: 'bold',
          marginLeft: 10,
        }}>{label}</Text>
      </View>

      <View style={{
        flexDirection: 'row',
      }}>
        <Text style={{
          color: COLORS.white,
          fontWeight: 'bold',
          marginLeft: 10,
        }}>{amount}</Text>
        <Text style={{
          color: COLORS.white,
          fontWeight: 'bold',
          marginLeft: 10,
          ...symbolTextStyle,
        }}>{symbol}</Text>
      </View>

    </View>
  )
}

const Overview = () => {

  const [hide, setHide] = React.useState(true)

  function toggleHide(){
    setHide(!hide)
  }

  function renderHeader() {
    return (
      <View style={{
        height: 130,
        backgroundColor: COLORS.primary_back,
        marginTop: 60,
        borderTopRightRadius: 30,
        borderTopLeftRadius: 30,
      }}>

        <View style={{
          marginHorizontal: 20,
        }}>

          {/* TotalPrice */}
          <View style={{
            flexDirection: 'row',
            alignItems: 'center',
          }}>
            <Text style={{
              color: COLORS.gray2,
              letterSpacing: 0.5,
              fontSize: 12,
              marginTop: 15,
              fontWeight: '600',
            }}>Total Value(BTC)</Text>

            <TouchableOpacity onPress={toggleHide} style={{ marginLeft: 7 }}>
              <Image style={{
                marginTop: 16,
                height: 15,
                width: 15,
                tintColor: COLORS.gray2,
              }} source={hide ? icons.eye : icons.eye_off} />

            </TouchableOpacity>
          </View>


          <View style={{
            flexDirection: 'row',
            alignItems: 'center',
          }}>
            <Text style={{
              fontWeight: '600',
              fontSize: 30,
              color: COLORS.white,
            }}>{!hide ? "***" : "0.00" }</Text>
            <Text style={{
              color: COLORS.gray2,
              letterSpacing: 0.5,
              fontSize: 12,
              marginLeft: 10,
              fontWeight: 'bold',
            }}>≈</Text>

            <Text style={{
              color: COLORS.gray2,
              letterSpacing: 0.5,
              fontSize: 12,
              marginLeft: 10,
              fontWeight: 'bold',
            }}>$0.00</Text>
          </View>


          {/* Buttons */}
          <View style={{
            flexDirection: 'row',
            marginTop: 20
          }}>
            <TextButton
              containerStyle={{
                width: '31%',
                height: 30,
                marginRight: 10,
              }}
              label={'Deposit'}

            />
            <TextButton
              containerStyle={{
                width: '31%',
                height: 30,
              }}
              label={'Withdraw'}
            />
            <TextButton
              containerStyle={{
                width: '31%',
                height: 30,
                marginLeft: 10,
              }}
              label={'Transfer'}
            />
          </View>

        </View>



      </View>
    )
  }

  function renderPortfolio() {
    return (
      <View style={{
        flex: 1,
        backgroundColor: COLORS.primary_back,
        marginTop: 10,
      }}>

        <View style={{
          marginHorizontal: 20,
          marginTop: 20,
        }}>
          <Text style={{
            fontSize: 20,
            fontWeight: 'bold',
            color: COLORS.white,
            letterSpacing: 0.9,
          }}>Portfolio</Text>


          <View style={{
            marginTop: 20,
          }}>
            <Portfolio icon={icons.spot} label={"Spot"} amount={!hide ? "***" : "0.00" } symbol={"BTC"} />
            <Portfolio icon={icons.fund} label={"Funding"} amount={!hide ? "***" : "0.00" } symbol={"BTC"} />
            <Portfolio icon={icons.margin} label={"Cross Margin"} amount={!hide ? "***" : "0.00" } symbol={"BTC"} />
            <Portfolio icon={icons.up_margin} label={"Ioslated Margin"} amount={!hide ? "***" : "0.00" } symbol={"BTC"} />
            <Portfolio icon={icons.usd_note} label={"USDⓢ-M Futures"} symbol={"Activate"} symbolTextStyle={{
              color: COLORS.primary
            }} />
            <Portfolio icon={icons.coin} label={"COIN-M Futures"} symbol={"Activate"} symbolTextStyle={{
              color: COLORS.primary
            }} />
            <Portfolio icon={icons.pig} label={"Earn"} amount={!hide ? "***" : "0.00" } symbol={"BTC"} />
            <Portfolio icon={icons.dig} label={"Pool"} amount={!hide ? "***" : "0.00" } symbol={"BTC"} />
            <Portfolio icon={icons.spot} label={"T-Option"} amount={!hide ? "***" : "0.00" } symbol={"BTC"} />
            <Portfolio icon={icons.wallet} label={"Third-party Account"} amount={!hide ? "***" : "0.00" } symbol={"BTC"} />

          </View>
        </View>

      </View>
    )
  }
  return (
    <View style={{
      flex: 1,
      backgroundColor: COLORS.black,
    }}>


      {/* Header */}
      {renderHeader()}


      {/* Portfolio */}
      {renderPortfolio()}
    </View>
  )
}

export default Overview

const styles = StyleSheet.create({})