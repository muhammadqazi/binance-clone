import { StyleSheet, Text, View, TouchableOpacity, Image } from 'react-native'
import React from 'react'
import { COLORS, icons } from '../../../../constants'
import { TextButton } from '../../../../components'

const Futures = () => {


  const [Tab, setTab] = React.useState(true)
  const [hide, setHide] = React.useState(true)
  const [hideAssets, sethideAssets] = React.useState(true)
  const [showDept, setShowDept] = React.useState(true)
  const [tabs3, setTabs3] = React.useState(1)

  function toggleHide() {
    setHide(!hide)
  }

  function toggleHideAssets() {
    sethideAssets(!hideAssets)
  }

  function toggleShowDept() {
    setShowDept(!showDept)
  }


  function renderBalance() {
    return (
      <View style={{
        marginHorizontal: 20
      }}>
        {/* TotalPrice */}
        <View style={{
          flexDirection: 'row',
          alignItems: 'center',
        }}>
          <Text style={{
            color: COLORS.gray2,
            letterSpacing: 0.5,
            fontSize: 12,
            marginTop: 15,
            fontWeight: '600',
          }}>Total Balance(USD)</Text>

          <TouchableOpacity onPress={toggleHide} style={{ marginLeft: 7 }}>
            <Image style={{
              marginTop: 16,
              height: 15,
              width: 15,
              tintColor: COLORS.gray2,
            }} source={hide ? icons.eye : icons.eye_off} />

          </TouchableOpacity>
        </View>

        {/* 0.00*** */}
        <View style={{
          flexDirection: 'row',
          alignItems: 'center',
        }}>
          <Text style={{
            fontWeight: '600',
            fontSize: 30,
            color: COLORS.white,
          }}>{!hide ? "*.**" : "0.00"}</Text>
          <Text style={{
            color: COLORS.gray2,
            letterSpacing: 0.5,
            fontSize: 12,
            marginLeft: 10,
            fontWeight: 'bold',
          }}>≈</Text>

          <Text style={{
            color: COLORS.gray2,
            letterSpacing: 0.5,
            fontSize: 12,
            marginLeft: 10,
            fontWeight: 'bold',
          }}>${!hide ? " *.**" : "0.0000"}</Text>
        </View>


        {/* Primary and green buttons */}
        <View style={{
          marginTop: 10,
          flexDirection: 'row',
        }}>

          <TouchableOpacity style={{
            width: 90,
            height: 20,
            alignItems: 'center',
            justifyContent: 'center',
            borderWidth: 0.4,
            borderLeftColor: COLORS.primary,
            borderTopColor: COLORS.primary,
            borderBottomColor: COLORS.primary,
          }}>
            <Text style={{
              fontSize: 12,
              color: COLORS.primary,
              fontWeight: 'bold',
            }}>PNL Analysis</Text>
          </TouchableOpacity>



          <TouchableOpacity style={{
            width: 90,
            height: 20,
            justifyContent: 'center',
            borderWidth: 0.4,
            borderLeftColor: COLORS.green,
            borderTopColor: COLORS.green,
            borderBottomColor: COLORS.green,
            borderRightColor: COLORS.green,
            flexDirection: 'row',
            alignItems: 'center',
          }}>
            <Text style={{
              fontSize: 12,
              color: COLORS.green,
              fontWeight: 'bold',
              marginRight: 4
            }}>0 Buy Wins</Text>

            <Image source={icons.forward} style={{
              height: 10,
              width: 10,
              tintColor: COLORS.green,
            }} />
          </TouchableOpacity>

        </View>


      </View>
    )
  }

  function renderButtons() {
    return (
      <View style={{
        marginHorizontal: 20,
        flexDirection: 'row',
        marginTop: 20
      }}>
        <TextButton
          containerStyle={{
            width: '31%',
            height: 30,
            marginRight: 10,
            backgroundColor: COLORS.darkGray
          }}
          label={'Borrow / Repay'}
          labelStyle={{
            color: COLORS.white,
          }}

        />
        <TextButton
          containerStyle={{
            width: '31%',
            height: 30,
            backgroundColor: COLORS.darkGray

          }}
          label={'Convert'}
          labelStyle={{
            color: COLORS.white,
          }}
        />
        <TextButton
          containerStyle={{
            width: '31%',
            height: 30,
            marginLeft: 10,
            backgroundColor: COLORS.darkGray

          }}
          label={'Transfer'}
          labelStyle={{
            color: COLORS.white,
          }}
        />
      </View>
    )
  }

  function renderBoxes(col1, col2, marginTop) {
    return (
      <View style={{
        flexDirection: 'row',
        marginTop: marginTop,
      }}>
        {/* Box 1 */}
        <View style={{
          width: '50%',
          height: 70,
          justifyContent: 'center',
          borderWidth: 0.4,
          borderTopColor: COLORS.darkGray,
          borderBottomColor: COLORS.darkGray,
          borderRightColor: COLORS.darkGray,
          paddingHorizontal: 20,
          paddingVertical: 20,
        }}>
          <Text style={{
            fontWeight: '600',
            fontSize: 10,
            color: COLORS.darkGray,
            letterSpacing: 0.5,
            marginBottom: 5
          }}>{col1}</Text>
          <Text style={{
            fontWeight: '600',
            fontSize: 11,
            color: COLORS.white,
            letterSpacing: 0.5,
          }}>{!hide ? "*.**" : "0.00"} BTC</Text>

          <View style={{
            flexDirection: 'row',
            marginTop: 5
          }}>
            <Text style={{
              color: COLORS.gray2,
              letterSpacing: 0.5,
              fontSize: 11,
              fontWeight: 'bold',
            }}>≈</Text>

            <Text style={{
              color: COLORS.gray2,
              letterSpacing: 0.5,
              fontSize: 11,
              fontWeight: 'bold',
            }}> ${!hide ? " *.**" : "0.0000"}</Text>
          </View>

        </View>

        {/* Box 2 */}
        <View style={{
          width: '50%',
          justifyContent: 'center',
          height: 70,
          borderWidth: 0.4,
          borderTopColor: COLORS.darkGray,
          borderBottomColor: COLORS.darkGray,
          borderRightColor: COLORS.darkGray,
          paddingHorizontal: 20,
          paddingVertical: 20,
        }}>
          <Text style={{
            fontWeight: '600',
            fontSize: 10,
            color: COLORS.darkGray,
            letterSpacing: 0.5,
            marginBottom: 5
          }}>{col2}</Text>
          <Text style={{
            fontWeight: '600',
            fontSize: 11,
            color: COLORS.white,
            letterSpacing: 0.5,
          }}>{!hide ? "*.**" : "0.00"} BTC</Text>

          <View style={{
            flexDirection: 'row',
            marginTop: 5
          }}>
            <Text style={{
              color: COLORS.gray2,
              letterSpacing: 0.5,
              fontSize: 11,
              fontWeight: 'bold',
            }}>≈</Text>

            <Text style={{
              color: COLORS.gray2,
              letterSpacing: 0.5,
              fontSize: 11,
              fontWeight: 'bold',
            }}> ${!hide ? " *.**" : "0.0000"}</Text>
          </View>

        </View>

      </View>



    )

  }


  function renderHeader() {
    return (
      <View style={{
        height: 450,
        backgroundColor: COLORS.primary_back,
        marginTop: 60,
        borderTopRightRadius: 30,
        borderTopLeftRadius: 30,
      }}>

        {/* Tabs */}
        {renderTabs()}

        {/* Balance Show */}
        {renderBalance()}

        {/* Boxes */}
        {renderBoxes("Margin Balance (USD)", "Wallet Balance (USD)", 20)}
        {renderBoxes("Total Unrealized PNL (USD)", "Total Cross Collateral (USD)")}
        {renderBoxes("Total Borrowed (USD)", "Total Interest (USD)")}

        {/* Buttons */}
        {renderButtons()}



      </View>
    )
  }

  function renderPortfolio() {

    function renderTabs() {
      return (
        <View>
          <View style={{
            marginHorizontal: 20,
            marginTop: 20,
            flexDirection: 'row'
          }}>

            {/* 1 */}
            <TouchableOpacity onPress={() => setTabs3(1)}>
              <Text style={{
                fontWeight: "600",
                fontSize: 15,
                color: tabs3 == 1 ? COLORS.white : COLORS.darkGray,
              }}>Positions</Text>

              {tabs3 == 1 &&
                <View style={{
                  alignItems: 'center',
                  width: 70
                }}>
                  <View style={{
                    backgroundColor: COLORS.primary,
                    width: 30,
                    height: 3,
                    marginTop: 5,
                    borderRadius: 1.5
                  }} />
                </View>
              }

            </TouchableOpacity>
            {/* 2 */}
            <TouchableOpacity onPress={() => setTabs3(2)} style={{
              marginHorizontal: 20
            }}>
              <Text style={{
                fontWeight: "600",
                fontSize: 15,
                color: tabs3 == 2 ? COLORS.white : COLORS.darkGray,
              }}>Assets</Text>
              {tabs3 == 2 &&
                <View style={{
                  alignItems: 'center',
                  width: 51
                }}>
                  <View style={{
                    backgroundColor: COLORS.primary,
                    width: 30,
                    height: 3,
                    marginTop: 5,
                    borderRadius: 1.5
                  }} />
                </View>
              }
            </TouchableOpacity>
            {/* 3 */}
            <TouchableOpacity onPress={() => setTabs3(3)}>
              <Text style={{
                fontWeight: "600",
                fontSize: 15,
                color: tabs3 == 3 ? COLORS.white : COLORS.darkGray,
              }}>Collaterals</Text>
              {tabs3 == 3 &&
                <View style={{
                  alignItems: 'center',
                  width: 70
                }}>
                  <View style={{
                    backgroundColor: COLORS.primary,
                    width: 30,
                    height: 3,
                    marginTop: 5,
                    borderRadius: 1.5
                  }} />
                </View>
              }
            </TouchableOpacity>


          </View>
        </View>
      )
    }
    return (
      <View style={{
        flex: 1,
        backgroundColor: COLORS.primary_back,
        marginTop: 10,
      }}>

        {/* Prompt */}
        <View style={{
          marginHorizontal: 20,
          marginTop: 20,
        }}>
          <TouchableOpacity style={{
            backgroundColor: COLORS.primary_back,
            borderRadius: 3,
            height: 40,
            alignItems: 'center',
            justifyContent: "space-between",
            paddingHorizontal: 15,
            flexDirection: 'row',
            ...styles.shadow
          }}>
            <Text style={{
              fontWeight: '600',
              color: COLORS.white
            }}>
              Use BNB for fees (10% discount)
            </Text>

            <Image source={icons.information} style={{
              height: 20,
              width: 20,
              tintColor: COLORS.gray
            }} />

          </TouchableOpacity>

        </View>

        {renderTabs()}


        <View style={{
          backgroundColor: COLORS.darkGray,
          height: 0.5
        }} />


        {tabs3 == 1 &&

          <View style={{
            flexDirection: 'row',
            justifyContent: 'space-between',
            marginTop: 20,
          }}>

            <View style={{
              flexDirection: 'row',
            }}>
              <TouchableOpacity style={{
                height: 23,
                width: 82,
                borderRadius: 3,
                backgroundColor: Tab ? COLORS.darkGray : COLORS.primary_back,
                marginLeft: 20,
                marginRight: 8,
                justifyContent: 'center',
                alignItems: 'center',
              }}
                onPress={() => { setTab(true) }}
              >

                <Text style={{
                  color: Tab ? COLORS.white : COLORS.darkGray,
                  fontWeight: '400',
                }}>USDⓢ-M</Text>


              </TouchableOpacity>
              <TouchableOpacity style={{
                height: 23,
                width: 82,
                borderRadius: 3,
                backgroundColor: Tab ? COLORS.primary_back : COLORS.darkGray,
                justifyContent: 'center',
                alignItems: 'center',
              }}
                onPress={() => setTab(false)}
              >

                <Text style={{
                  color: Tab ? COLORS.darkGray : COLORS.white,
                  fontWeight: '400',
                }}>Options</Text>


              </TouchableOpacity>
            </View>

          </View>
        }
        {tabs3 == 2 &&
          <View style={{
            marginHorizontal: 20,
            marginTop: 20,
          }}>
            <View style={{
              flexDirection: 'row',
              alignItems: 'center',
            }}>
              <TouchableOpacity onPress={toggleHideAssets}>
                <Image source={icons.tick} style={{
                  width: 15,
                  height: 15,
                  tintColor: hideAssets ? COLORS.primary : COLORS.darkGray
                }} />
              </TouchableOpacity>


              <Text style={{
                color: COLORS.darkGray,
                marginLeft: 4,
                fontSize: 12,
              }}>Hide 0 balances</Text>
            </View>
            {/* Row1 */}

            {!hideAssets &&
              <View style={{
                marginTop: 20
              }}>
                <View style={{
                  flexDirection: 'row',
                  alignItems: 'center',

                }}>
                  <Image source={icons.aave} style={{
                    height: 25,
                    width: 25,
                  }} />

                  <Text style={{
                    color: COLORS.white,
                    fontWeight: 'bold',
                    marginLeft: 10
                  }}>AAVE</Text>
                </View>
                {/* Row2 */}
                <View style={{
                  marginTop: 10,
                  flexDirection: 'row',
                  justifyContent: 'space-between',
                }}>
                  <View>
                    <Text style={{
                      color: COLORS.darkGray,
                    }}>Wallet Balance</Text>
                    <Text style={{
                      color: COLORS.white,
                      fontWeight: 'bold',
                      marginTop: 5
                    }}>0.0000</Text>
                  </View>
                  <View>
                    <Text style={{
                      color: COLORS.darkGray,
                    }}>Unrealized PNL</Text>
                    <Text style={{
                      color: COLORS.white,
                      fontWeight: 'bold',
                      marginTop: 5
                    }}>0.0000</Text>
                  </View>
                </View>
                {/* Row3 */}
                <View style={{
                  marginTop: 10,
                  flexDirection: 'row',
                  justifyContent: 'space-between',
                }}>
                  <View>
                    <Text style={{
                      color: COLORS.darkGray,
                    }}>Wallet Balance</Text>
                    <Text style={{
                      color: COLORS.white,
                      fontWeight: 'bold',
                      marginTop: 5
                    }}>0.0000</Text>
                  </View>
                  <View>
                    <Text style={{
                      color: COLORS.darkGray,
                    }}>Unrealized PNL</Text>
                    <Text style={{
                      color: COLORS.white,
                      fontWeight: 'bold',
                      marginTop: 5
                    }}>0.0000</Text>
                  </View>
                </View>

              </View>
            }

          </View>
        }

        {tabs3 == 3 &&
          <View style={{
            marginHorizontal: 30,
            marginTop: 20,
          }}>
            <View style={{
              flexDirection: 'row',
              alignItems: 'center',
            }}>
              <Image source={icons.aave} style={{
                height: 25,
                width: 25,
              }} />
              <Text style={{
                fontWeight: 'bold',
                fontSize: 16,
                marginLeft: 6,
                color: COLORS.white
              }}>AAVE</Text>
            </View>

            <View style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
            }}>
              <View style={{
                marginTop: 10
              }}>
                <Text style={{
                  color: COLORS.darkGray,
                }}>LTV</Text>
                <Text style={{
                  color: COLORS.green,
                  marginTop: 5
                }}>0.00%</Text>
              </View>

              <View style={{
                marginTop: 10
              }}>
                <Text style={{
                  color: COLORS.darkGray,
                }}>Locked</Text>
                <Text style={{
                  color: COLORS.white,
                  marginTop: 5
                }}>0.000000</Text>
              </View>
            </View>
          </View>
        }


      </View>
    )
  }

  function renderTabs() {
    return (
      <View style={{
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginTop: 20,
      }}>

        <View style={{
          flexDirection: 'row',
        }}>
          <TouchableOpacity style={{
            height: 23,
            width: 82,
            borderRadius: 3,
            backgroundColor: Tab ? COLORS.darkGray : COLORS.primary_back,
            marginLeft: 20,
            marginRight: 8,
            justifyContent: 'center',
            alignItems: 'center',
          }}
            onPress={() => { setTab(true) }}
          >

            <Text style={{
              color: Tab ? COLORS.white : COLORS.darkGray,
              fontWeight: '400',
            }}>USDⓢ-M</Text>


          </TouchableOpacity>
          <TouchableOpacity style={{
            height: 23,
            width: 82,
            borderRadius: 3,
            backgroundColor: Tab ? COLORS.primary_back : COLORS.darkGray,
            justifyContent: 'center',
            alignItems: 'center',
          }}
            onPress={() => setTab(false)}
          >

            <Text style={{
              color: Tab ? COLORS.darkGray : COLORS.white,
              fontWeight: '400',
            }}>COIN-M</Text>


          </TouchableOpacity>
        </View>




        <TouchableOpacity style={{
          marginRight: 20
        }}>
          <Image source={icons.timer} style={{
            height: 20,
            width: 20,
            tintColor: COLORS.darkGray,
          }} />
        </TouchableOpacity>




      </View>
    )
  }
  return (
    <View style={{
      flex: 1,
      backgroundColor: COLORS.black,
    }}>


      {/* Header */}
      {renderHeader()}


      {/* Portfolio */}
      {renderPortfolio()}
    </View>
  )
}

export default Futures

const styles = StyleSheet.create({
  shadow: {
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 4
    },
    shadowOpacity: 0.30,
    shadowRadius: 4.65,

    elevation: 8
  }
})