import { StyleSheet, Text, View, TouchableOpacity, Image } from 'react-native'
import React from 'react'
import { COLORS, icons } from '../../../../constants'
import { TextButton } from '../../../../components'


const PortfolioCoin = ({ icon, name, token, balance, available, freeze }) => {
    return (
        <View style={{
            marginTop: 35,
            marginHorizontal: 20,
        }}>

            <View style={{
                flexDirection: 'row',
                justifyContent: 'space-between',
            }}>

                <View style={{
                    flexDirection: 'row'
                }}>
                    <Image source={icon} style={{
                        height: 22,
                        width: 22,
                    }} />

                    <View style={{
                        paddingLeft: 10,
                    }}>
                        <Text style={{
                            color: COLORS.white,
                            fontWeight: 'bold',
                            fontSize: 16,
                        }}>{name}</Text>

                        <Text style={{
                            color: COLORS.darkGray,
                            fontSize: 12
                        }}>{token}</Text>
                    </View>
                </View>

                <Text style={{
                    color: COLORS.white,
                    fontWeight: 'bold',
                    fontSize: 16,
                }}>{balance}</Text>

            </View>

            <View style={{
                marginTop: 20,
                flexDirection: 'row',
                justifyContent: 'space-between',
            }}>

                <View style={{
                    marginLeft: 32
                }}>
                    <Text style={{
                        color: COLORS.darkGray,
                    }}>Available</Text>
                    <Text style={{
                        color: COLORS.white,
                        marginTop: 5
                    }}>{available}</Text>
                </View>

                <View>
                    <Text style={{
                        color: COLORS.darkGray,
                    }}>Freeze</Text>
                    <Text style={{
                        color: COLORS.white,
                        marginTop: 5
                    }}>{freeze}</Text>
                </View>

            </View>

            <View style={{
                backgroundColor: COLORS.darkGray,
                height: 0.4,
                marginTop: 20
            }} />

        </View>
    )
}


const Margin = () => {


    const [Tab, setTab] = React.useState(true)
    const [hide, setHide] = React.useState(true)
    const [hideAssets, sethideAssets] = React.useState(true)
    const [showDept, setShowDept] = React.useState(true)

    function toggleHide() {
        setHide(!hide)
    }

    function toggleHideAssets() {
        sethideAssets(!hideAssets)
    }

    function toggleShowDept() {
        setShowDept(!showDept)
    }




    function renderHeader() {
        return (
            <View style={{
                height: 370,
                backgroundColor: COLORS.primary_back,
                marginTop: 60,
                borderTopRightRadius: 30,
                borderTopLeftRadius: 30,
            }}>


                {renderTabs()}

                <View style={{
                    marginHorizontal: 20
                }}>
                    {/* TotalPrice */}
                    <View style={{
                        flexDirection: 'row',
                        alignItems: 'center',
                    }}>
                        <Text style={{
                            color: COLORS.gray2,
                            letterSpacing: 0.5,
                            fontSize: 12,
                            marginTop: 15,
                            fontWeight: '600',
                        }}>Equity Value(BTC)</Text>

                        <TouchableOpacity onPress={toggleHide} style={{ marginLeft: 7 }}>
                            <Image style={{
                                marginTop: 16,
                                height: 15,
                                width: 15,
                                tintColor: COLORS.gray2,
                            }} source={hide ? icons.eye : icons.eye_off} />

                        </TouchableOpacity>
                    </View>

                    {/* *** */}
                    <View style={{
                        flexDirection: 'row',
                        alignItems: 'center',
                    }}>
                        <Text style={{
                            fontWeight: '600',
                            fontSize: 30,
                            color: COLORS.white,
                        }}>{!hide ? "*.**" : "0.00"} BTC</Text>
                        <Text style={{
                            color: COLORS.gray2,
                            letterSpacing: 0.5,
                            fontSize: 12,
                            marginLeft: 10,
                            fontWeight: 'bold',
                        }}>≈</Text>

                        <Text style={{
                            color: COLORS.gray2,
                            letterSpacing: 0.5,
                            fontSize: 12,
                            marginLeft: 10,
                            fontWeight: 'bold',
                        }}>${!hide ? " *.**" : "0.0000"}</Text>
                    </View>
                    {/* 7d PNL */}
                    <View>
                        <Text style={{
                            color: COLORS.gray2,
                            letterSpacing: 0.5,
                            fontSize: 12,
                            marginTop: 15,
                            fontWeight: '600',
                        }}>7d PNL (since 2022-02-16 11:26)</Text>

                        <View style={{
                            flexDirection: 'row',
                            alignItems: 'center',
                            marginTop: 10
                        }}>
                            <Text style={{
                                fontWeight: '600',
                                fontSize: 15,
                                color: COLORS.white,
                            }}>{!hide ? "*.**" : "0.00"} BTC</Text>
                            <Text style={{
                                color: COLORS.darkGray,
                                letterSpacing: 0.5,
                                fontSize: 12,
                                marginLeft: 10,
                                fontWeight: 'bold',
                            }}>/</Text>

                            <Text style={{
                                color: COLORS.white,
                                letterSpacing: 0.5,
                                fontSize: 15,
                                marginLeft: 10,
                                fontWeight: 'bold',
                            }}>${!hide ? "*.**" : "0.00"}</Text>
                        </View>
                    </View>

                </View>

                {/* Boxes */}
                <View style={{
                    flexDirection: 'row',
                    marginTop: 20,
                }}>
                    {/* Box 1 */}
                    <View style={{
                        width: '50%',
                        height: 100,
                        borderWidth: 0.4,
                        borderTopColor: COLORS.darkGray,
                        borderBottomColor: COLORS.darkGray,
                        borderRightColor: COLORS.darkGray,
                        paddingHorizontal: 20,
                        paddingVertical: 20,
                    }}>
                        <Text style={{
                            fontWeight: '600',
                            fontSize: 11,
                            color: COLORS.darkGray,
                            letterSpacing: 0.5,
                            marginBottom: 5
                        }}>Total Dept</Text>
                        <Text style={{
                            fontWeight: '600',
                            fontSize: 11,
                            color: COLORS.white,
                            letterSpacing: 0.5,
                        }}>{!hide ? "*.**" : "0.00"} BTC</Text>

                        <View style={{
                            flexDirection: 'row',
                            marginTop: 5
                        }}>
                            <Text style={{
                                color: COLORS.gray2,
                                letterSpacing: 0.5,
                                fontSize: 11,
                                fontWeight: 'bold',
                            }}>≈</Text>

                            <Text style={{
                                color: COLORS.gray2,
                                letterSpacing: 0.5,
                                fontSize: 11,
                                fontWeight: 'bold',
                            }}> ${!hide ? " *.**" : "0.0000"}</Text>
                        </View>

                    </View>

                    {/* Box 2 */}
                    <View style={{
                        width: '50%',
                        height: 100,
                        borderWidth: 0.4,
                        borderTopColor: COLORS.darkGray,
                        borderBottomColor: COLORS.darkGray,
                        borderRightColor: COLORS.darkGray,
                        paddingHorizontal: 20,
                        paddingVertical: 20,
                    }}>
                        <Text style={{
                            fontWeight: '600',
                            fontSize: 11,
                            color: COLORS.darkGray,
                            letterSpacing: 0.5,
                            marginBottom: 5
                        }}>Amount Equity</Text>
                        <Text style={{
                            fontWeight: '600',
                            fontSize: 11,
                            color: COLORS.white,
                            letterSpacing: 0.5,
                        }}>{!hide ? "*.**" : "0.00"} BTC</Text>

                        <View style={{
                            flexDirection: 'row',
                            marginTop: 5
                        }}>
                            <Text style={{
                                color: COLORS.gray2,
                                letterSpacing: 0.5,
                                fontSize: 11,
                                fontWeight: 'bold',
                            }}>≈</Text>

                            <Text style={{
                                color: COLORS.gray2,
                                letterSpacing: 0.5,
                                fontSize: 11,
                                fontWeight: 'bold',
                            }}> ${!hide ? " *.**" : "0.0000"}</Text>
                        </View>

                    </View>

                </View>

                {/* Buttons */}

                <View style={{
                    marginHorizontal: 20,
                    flexDirection: 'row',
                    marginTop: 20
                }}>
                    <TextButton
                        containerStyle={{
                            width: '31%',
                            height: 30,
                            marginRight: 10,
                            backgroundColor: COLORS.darkGray
                        }}
                        label={'Borrow'}
                        labelStyle={{
                            color: COLORS.white,
                        }}

                    />
                    <TextButton
                        containerStyle={{
                            width: '31%',
                            height: 30,
                            backgroundColor: COLORS.darkGray

                        }}
                        label={'Repay'}
                        labelStyle={{
                            color: COLORS.white,
                        }}
                    />
                    <TextButton
                        containerStyle={{
                            width: '31%',
                            height: 30,
                            marginLeft: 10,
                            backgroundColor: COLORS.darkGray

                        }}
                        label={'Transfer'}
                        labelStyle={{
                            color: COLORS.white,
                        }}
                    />
                </View>



            </View>
        )
    }

    function renderPortfolio() {
        return (
            <View style={{
                flex: 1,
                backgroundColor: COLORS.primary_back,
                marginTop: 10,
            }}>

                {/* Hide */}
                <View style={{
                    marginHorizontal: 20,
                    marginTop: 20,
                }}>
                    <TouchableOpacity style={{
                        backgroundColor: COLORS.darkGray,
                        borderRadius: 3,
                        height: 40,
                        justifyContent: 'center',
                        paddingHorizontal: 15,
                        ...styles.shadow
                    }}>
                        <Text style={{
                            fontWeight: '600',
                            color: COLORS.primary
                        }}>
                            Convert small assets to BNB
                        </Text>

                    </TouchableOpacity>

                    <View style={{
                        flexDirection: 'row',
                        marginTop: 20,
                    }}>
                        <View style={{
                            flexDirection: 'row',
                            alignItems: 'center',
                        }}>
                            <TouchableOpacity onPress={toggleHideAssets}>
                                <Image source={icons.tick} style={{
                                    width: 15,
                                    height: 15,
                                    tintColor: hideAssets ? COLORS.primary : COLORS.darkGray
                                }} />
                            </TouchableOpacity>


                            <Text style={{
                                color: COLORS.darkGray,
                                marginLeft: 4,
                                fontSize: 12,
                            }}>Hide small accounts</Text>
                        </View>
                        {/* 2 */}
                        <View style={{
                            flexDirection: 'row',
                            alignItems: 'center',
                            marginLeft: 10
                        }}>

                            <TouchableOpacity onPress={toggleShowDept}>
                                <Image source={icons.tick} style={{
                                    width: 15,
                                    height: 15,
                                    tintColor: showDept ? COLORS.primary : COLORS.darkGray
                                }} />
                            </TouchableOpacity>


                            <Text style={{
                                color: COLORS.darkGray,
                                marginLeft: 4,
                                fontSize: 12,
                            }}>Only show depts</Text>
                        </View>

                    </View>

                    {/* Coin */}

                    {!hideAssets &&
                        <PortfolioCoin
                            icon={icons.aave}
                            name={'AAVE'}
                            token={'Aave'}
                            balance={'0.00'}
                            available={'0.00'}
                            freeze={'0.00'}
                        />
                    }


                </View>

            </View>
        )
    }

    function renderTabs() {
        return (
            <View style={{
                flexDirection: 'row',
                justifyContent: 'space-between',
                marginTop: 20,
            }}>

                <View style={{
                    flexDirection: 'row',
                }}>
                    <TouchableOpacity style={{
                        height: 30,
                        width: 60,
                        borderRadius: 3,
                        backgroundColor: Tab ? COLORS.darkGray : COLORS.primary_back,
                        marginLeft: 20,
                        marginRight: 8,
                        justifyContent: 'center',
                        alignItems: 'center',
                    }}
                        onPress={() => { setTab(true) }}
                    >

                        <Text style={{
                            color: Tab ? COLORS.white : COLORS.darkGray,
                            fontWeight: '400',
                        }}>Cross</Text>


                    </TouchableOpacity>
                    <TouchableOpacity style={{
                        height: 30,
                        width: 70,
                        borderRadius: 3,
                        backgroundColor: Tab ? COLORS.primary_back : COLORS.darkGray,
                        justifyContent: 'center',
                        alignItems: 'center',
                    }}
                        onPress={() => setTab(false)}
                    >

                        <Text style={{
                            color: Tab ? COLORS.darkGray : COLORS.white,
                            fontWeight: '400',
                        }}>Ioslated</Text>


                    </TouchableOpacity>
                </View>

                <View style={{
                    flexDirection: 'row',
                    justifyContent: 'space-evenly',
                    width: 100,
                }}>
                    <TouchableOpacity>
                        <Image source={icons.flower} style={{
                            height: 20,
                            width: 20,
                            tintColor: COLORS.darkGray,
                        }} />
                    </TouchableOpacity>

                    <TouchableOpacity>
                        <Image source={icons.timer} style={{
                            height: 20,
                            width: 20,
                            tintColor: COLORS.darkGray,
                        }} />
                    </TouchableOpacity>

                    <TouchableOpacity>
                        <Image source={icons.filter} style={{
                            height: 20,
                            width: 20,
                            tintColor: COLORS.darkGray,
                        }} />
                    </TouchableOpacity>


                </View>

            </View>
        )
    }
    return (
        <View style={{
            flex: 1,
            backgroundColor: COLORS.black,
        }}>


            {/* Header */}
            {renderHeader()}


            {/* Portfolio */}
            {renderPortfolio()}
        </View>
    )
}

export default Margin

const styles = StyleSheet.create({})