import { StyleSheet, Text, View, TouchableOpacity, Image } from 'react-native'
import React from 'react'
import { COLORS, icons } from '../../../../constants'
import { TextButton } from '../../../../components'

const PortfolioCoin = ({ icon, name, token, balance, available, freeze }) => {
    return (
        <View style={{
            marginTop: 35,
            marginHorizontal: 20,
        }}>

            <View style={{
                flexDirection: 'row',
                justifyContent: 'space-between',
            }}>

                <View style={{
                    flexDirection: 'row'
                }}>
                    <Image source={icon} style={{
                        height: 22,
                        width: 22,
                    }} />

                    <View style={{
                        paddingLeft: 10,
                    }}>
                        <Text style={{
                            color: COLORS.white,
                            fontWeight: 'bold',
                            fontSize: 16,
                        }}>{name}</Text>

                        <Text style={{
                            color: COLORS.darkGray,
                            fontSize: 12
                        }}>{token}</Text>
                    </View>
                </View>

                <Text style={{
                    color: COLORS.white,
                    fontWeight: 'bold',
                    fontSize: 16,
                }}>{balance}</Text>

            </View>

            <View style={{
                marginTop: 20,
                flexDirection: 'row',
                justifyContent: 'space-between',
            }}>

                <View style={{
                    marginLeft:32
                }}>
                    <Text style={{
                        color: COLORS.darkGray,
                    }}>Available</Text>
                    <Text style={{
                        color: COLORS.white,
                        marginTop:5
                    }}>{available}</Text>
                </View>

                <View>
                    <Text style={{
                        color: COLORS.darkGray,
                    }}>Freeze</Text>
                    <Text style={{
                        color: COLORS.white,
                        marginTop:5
                    }}>{freeze}</Text>
                </View>

            </View>

            <View style={{
                backgroundColor:COLORS.darkGray,
                height:0.4,
                marginTop:20
            }}/>

        </View>
    )
}

const Funding = () => {

    const [hide, setHide] = React.useState(true)
    const [hidePromo, setHidePromo] = React.useState(true)
    const [hide0, setHide0] = React.useState(true)

    function toggleHide() {
        setHide(!hide)
    }

    function toggleHide0() {
        setHide0(!hide0)
    }
    function renderHeader() {
        return (
            <View style={{
                height: 130,
                backgroundColor: COLORS.primary_back,
                marginTop: 60,
                borderTopRightRadius: 30,
                borderTopLeftRadius: 30,
            }}>

                <View style={{
                    marginHorizontal: 20,
                }}>

                    {/* TotalPrice */}
                    <View style={{
                        flexDirection: 'row',
                        alignItems: 'center',
                    }}>
                        <Text style={{
                            color: COLORS.gray2,
                            letterSpacing: 0.5,
                            fontSize: 12,
                            marginTop: 15,
                            fontWeight: '600',
                        }}>Total Value(BTC)</Text>

                        <TouchableOpacity onPress={toggleHide} style={{ marginLeft: 7 }}>
                            <Image style={{
                                marginTop: 16,
                                height: 15,
                                width: 15,
                                tintColor: COLORS.gray2,
                            }} source={hide ? icons.eye : icons.eye_off} />

                        </TouchableOpacity>
                    </View>


                    <View style={{
                        flexDirection: 'row',
                        alignItems: 'center',
                    }}>
                        <Text style={{
                            fontWeight: '600',
                            fontSize: 30,
                            color: COLORS.white,
                        }}>{!hide ? "***" : "0.00"}</Text>
                        <Text style={{
                            color: COLORS.gray2,
                            letterSpacing: 0.5,
                            fontSize: 12,
                            marginLeft: 10,
                            fontWeight: 'bold',
                        }}>≈</Text>

                        <Text style={{
                            color: COLORS.gray2,
                            letterSpacing: 0.5,
                            fontSize: 12,
                            marginLeft: 10,
                            fontWeight: 'bold',
                        }}>$0.00</Text>
                    </View>


                    {/* Buttons */}
                    <View style={{
                        flexDirection: 'row',
                        marginTop: 20
                    }}>
                        <TextButton
                            containerStyle={{
                                width: '31%',
                                height: 30,
                                marginRight: 10,
                            }}
                            label={'Deposit'}

                        />
                        <TextButton
                            containerStyle={{
                                width: '31%',
                                height: 30,
                            }}
                            label={'Withdraw'}
                        />
                        <TextButton
                            containerStyle={{
                                width: '31%',
                                height: 30,
                                marginLeft: 10,
                            }}
                            label={'Transfer'}
                        />
                    </View>

                </View>



            </View>
        )
    }

    function renderPortfolio() {

        function renderCircle(icon) {
            return (
                <TouchableOpacity style={{
                    backgroundColor: COLORS.primary_back,
                    width: 50,
                    height: 50,
                    borderRadius: 90,
                    alignItems: "center",
                    justifyContent: "center",
                    ...styles.shadow
                }}>
                    <Image source={icon} style={{
                        height: 30,
                        width: 30,
                        tintColor: COLORS.primary
                    }} />
                </TouchableOpacity>
            )
        }

        function renderPromo() {
            return (
                <View style={{
                    marginHorizontal: 15,
                    marginTop: 25,
                    backgroundColor: COLORS.darkGray,
                    borderRadius: 3,
                    height: 60,
                    flexDirection: 'row',
                    justifyContent: 'space-between',
                    ...styles.shadow
                }}>

                    <View style={{
                        paddingHorizontal: 20,
                        paddingVertical: 10,
                    }}>
                        <Text style={{
                            color: COLORS.white,
                            fontSize: 14,
                            fontWeight: 'bold',
                            letterSpacing: 0.5,
                        }}>Funding Wallet</Text>

                        <Text style={{
                            color: COLORS.gray,
                            fontSize: 10,
                            marginTop: 5,
                            letterSpacing: 0.5,
                        }}>
                            Learn how to use the funding wallet
                        </Text>
                    </View>

                    <View style={{
                        flexDirection: 'row',
                        alignItems: 'center'
                    }}>
                        <Image source={icons.wallet} style={{
                            height: 30,
                            width: 30,
                            tintColor: COLORS.primary,
                            marginRight: 20,
                        }} />


                        <TouchableOpacity onPress={() => setHidePromo(false)}>
                            <Image source={icons.cross} style={{
                                height: 17,
                                width: 17,
                                marginRight: 10,
                                tintColor: COLORS.gray
                            }} />
                        </TouchableOpacity>
                    </View>

                </View>
            )
        }
        return (
            <View style={{
                flex: 1,
                backgroundColor: COLORS.primary_back,
                marginTop: 10,

            }}>

                <View style={{
                    marginHorizontal: 20,
                    marginTop: 20,
                    flexDirection: 'row',
                    justifyContent: 'space-evenly'
                }}>
                    {renderCircle(icons.p2p)}
                    {renderCircle(icons.creditcard)}
                    {renderCircle(icons.gift)}
                    {renderCircle(icons.dig)}

                </View>


                {hidePromo &&

                    renderPromo()

                }

                <View style={{
                    marginHorizontal: 20,
                    marginTop: 20,
                }}>
                    <Text style={{
                        color: COLORS.white,
                        fontSize: 17,
                        fontWeight: 'bold',
                        letterSpacing: 0.5,
                    }}>Available Balances</Text>

                    <View style={{
                        flexDirection: 'row',
                        marginTop: 20,
                        justifyContent: 'space-between',
                    }}>
                        <View style={{
                            flexDirection: 'row',
                        }}>
                            <TouchableOpacity onPress={toggleHide0}>
                                <Image source={icons.tick} style={{
                                    height: 15,
                                    width: 15,
                                    tintColor: hide0 ? COLORS.darkGray : COLORS.primary,
                                }} />

                            </TouchableOpacity>

                            <Text style={{
                                color: COLORS.darkGray,
                                fontSize: 12,
                                marginLeft: 10,
                            }}>Hide 0 Balances</Text>
                        </View>

                        <TouchableOpacity>
                            <Image source={icons.search} style={{
                                height: 15,
                                width: 15,
                                tintColor: COLORS.darkGray,
                            }} />
                        </TouchableOpacity>

                    </View>


                </View>

                <PortfolioCoin
                    icon={icons.aave}
                    name={'AAVE'}
                    token={'Aave'}
                    balance={'0.00'}
                    available={'0.00'}
                    freeze={'0.00'}
                />

            </View>
        )
    }
    return (
        <View style={{
            flex: 1,
            backgroundColor: COLORS.black,
        }}>


            {/* Header */}
            {renderHeader()}


            {/* Portfolio */}
            {renderPortfolio()}
        </View>
    )
}

export default Funding

const styles = StyleSheet.create({
    shadow: {
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 4
        },
        shadowOpacity: 0.30,
        shadowRadius: 4.65,

        elevation: 8
    }
})