import { StyleSheet, Text, View, TouchableOpacity, Image } from 'react-native'
import React from 'react'
import { COLORS, icons } from '../../../../constants'
import { TextButton } from '../../../../components'


const Portfolio = ({ icon, label, amount, inch, symbolTextStyle }) => {
    return (
        <View style={{
            marginTop: 35,
            flexDirection: 'row',
            alignItems: 'center',
            justifyContent: 'space-between',
        }}>

            <View style={{
                flexDirection: 'row',
            }}>
                <Image
                    source={icon}
                    style={{
                        height: 25,
                        width: 25,
                    }}
                />

                <View>
                    <Text style={{
                        color: COLORS.gray2,
                        fontWeight: 'bold',
                        marginLeft: 10,
                    }}>{label}</Text>
                    <Text style={{
                        color: COLORS.gray2,
                        fontWeight: 'bold',
                        marginLeft: 10,
                        fontSize: 9,
                        color: COLORS.darkGray,
                    }}>{inch}</Text>
                </View>

            </View>

            <View style={{
                flexDirection: 'row',
            }}>
                <Text style={{
                    color: COLORS.white,
                    fontWeight: 'bold',
                    marginLeft: 10,
                }}>{amount}</Text>

            </View>

        </View>
    )
}

const Spot = () => {

    const [hide, setHide] = React.useState(true)
    const [hideAmount, setHideAmount] = React.useState(true)

    function toggleHide() {
        setHide(!hide)
    }

    function toggleHideAmount() {
        setHideAmount(!hideAmount)
    }

    function renderHeader() {
        return (
            <View style={{
                height: 190,
                backgroundColor: COLORS.primary_back,
                marginTop: 60,
                borderTopRightRadius: 30,
                borderTopLeftRadius: 30,
            }}>

                <View style={{
                    marginHorizontal: 20,
                }}>

                    {/* TotalPrice */}
                    <View style={{
                        flexDirection: 'row',
                        alignItems: 'center',
                    }}>
                        <Text style={{
                            color: COLORS.gray2,
                            letterSpacing: 0.5,
                            fontSize: 12,
                            marginTop: 15,
                            fontWeight: '600',
                        }}>Total Value(BTC)</Text>

                        <TouchableOpacity onPress={toggleHideAmount} style={{ marginLeft: 7 }}>
                            <Image style={{
                                marginTop: 16,
                                height: 15,
                                width: 15,
                                tintColor: COLORS.gray2,
                            }} source={!hideAmount ? icons.eye : icons.eye_off} />

                        </TouchableOpacity>
                    </View>


                    <View style={{
                        flexDirection: 'row',
                        alignItems: 'center',
                    }}>
                        <Text style={{
                            fontWeight: '600',
                            fontSize: 30,
                            color: COLORS.white,
                        }}>{hideAmount ? "*.**" : "0.00" }</Text>
                        <Text style={{
                            color: COLORS.gray2,
                            letterSpacing: 0.5,
                            fontSize: 12,
                            marginLeft: 10,
                            fontWeight: 'bold',
                        }}>≈</Text>

                        <Text style={{
                            color: COLORS.gray2,
                            letterSpacing: 0.5,
                            fontSize: 12,
                            marginLeft: 10,
                            fontWeight: 'bold',
                        }}>$0.00</Text>
                    </View>

                    {/* Yesterdays PNL */}
                    <View style={{
                        flexDirection: 'row',
                        alignItems: 'center',
                    }}>
                        <Text style={{
                            color: COLORS.gray2,
                            letterSpacing: 0.5,
                            fontSize: 12,
                            marginTop: 15,
                            fontWeight: '600',
                        }}>Yesterday's PNL</Text>

                        <TouchableOpacity style={{ marginLeft: 7 }}>
                            <Image style={{
                                marginTop: 16,
                                height: 12,
                                width: 12,
                                tintColor: COLORS.gray2,
                            }} source={icons.information} />

                        </TouchableOpacity>
                    </View>

                    <View style={{
                        flexDirection: 'row',
                        alignItems: 'center',
                    }}>
                        <Text style={{
                            color: COLORS.gray2,
                            letterSpacing: 0.5,
                            fontSize: 12,
                            marginTop: 15,
                            fontWeight: '600',
                        }}>--</Text>

                        <TouchableOpacity style={{ marginLeft: 7 }}>
                            <Image style={{
                                marginTop: 16,
                                height: 10,
                                width: 10,
                                tintColor: COLORS.gray2,
                            }} source={icons.forward} />

                        </TouchableOpacity>
                    </View>



                    {/* Buttons */}
                    <View style={{
                        flexDirection: 'row',
                        marginTop: 20
                    }}>
                        <TextButton
                            containerStyle={{
                                width: '31%',
                                height: 30,
                                marginRight: 10,
                            }}
                            label={'Deposit'}

                        />
                        <TextButton
                            containerStyle={{
                                width: '31%',
                                height: 30,
                            }}
                            label={'Withdraw'}
                        />
                        <TextButton
                            containerStyle={{
                                width: '31%',
                                height: 30,
                                marginLeft: 10,
                            }}
                            label={'Transfer'}
                        />
                    </View>

                </View>



            </View>
        )
    }

    function renderPortfolio() {
        return (
            <View style={{
                flex: 1,
                backgroundColor: COLORS.primary_back,
                marginTop: 10,
            }}>

                <View style={{
                    marginHorizontal: 20,
                    marginTop: 20,
                }}>

                    {/* Convert */}
                    <TouchableOpacity style={{
                        backgroundColor: COLORS.darkGray,
                        borderRadius: 3,
                        height: 40,
                        justifyContent: 'center',
                        paddingHorizontal: 15,
                        ...styles.shadow
                    }}>
                        <Text style={{
                            fontWeight: '600',
                            color: COLORS.primary
                        }}>
                            Convert small assets to BNB
                        </Text>

                    </TouchableOpacity>

                    {/* Hide */}

                    <View style={{
                        flexDirection: 'row',
                        marginTop: 20,
                        justifyContent: 'space-between',
                    }}>
                        <View style={{
                            flexDirection: 'row',
                        }}>
                            <TouchableOpacity onPress={toggleHide}>
                                <Image source={icons.tick} style={{
                                    height: 15,
                                    width: 15,
                                    tintColor: hide ? COLORS.darkGray : COLORS.primary,
                                }} />

                            </TouchableOpacity>

                            <Text style={{
                                color: COLORS.darkGray,
                                fontSize: 12,
                                marginLeft: 10,
                            }}>Hide 0 Balances</Text>
                        </View>

                        <TouchableOpacity>
                            <Image source={icons.search} style={{
                                height: 15,
                                width: 15,
                                tintColor: COLORS.darkGray,
                            }} />
                        </TouchableOpacity>

                    </View>

                    {hide &&
                        <View style={{
                            marginTop: 20,
                        }}>
                            <Portfolio icon={icons.aave} label={"AAVE"} inch={"1inch"} amount={hideAmount ? "*.**" : "0.00" } />
                        </View>
                    }

                </View>

            </View>
        )
    }
    return (
        <View style={{
            flex: 1,
            backgroundColor: COLORS.black,
        }}>


            {/* Header */}
            {renderHeader()}


            {/* Portfolio */}
            {renderPortfolio()}
        </View>
    )
}

export default Spot

const styles = StyleSheet.create({
    shadow: {
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 4
        },
        shadowOpacity: 0.30,
        shadowRadius: 4.65,

        elevation: 8
    }
})
