import { StyleSheet, Text, View } from 'react-native'
import { createMaterialTopTabNavigator } from '@react-navigation/material-top-tabs';
import React from 'react'
import { Overview } from '../..';
import { COLORS } from '../../../constants';


const Tab = createMaterialTopTabNavigator();
const Tabs = () => {
    return (
        <View style={{
            flex: 1,
            backgroundColor: COLORS.primary_back,
            marginTop: 20
        }}>
            <Tab.Navigator
                sceneContainerStyle={{
                    backgroundColor: COLORS.primary
                }}
                keyboardDismissMode={'on-drag'}

                screenOptions={{
                    tabBarIndicatorContainerStyle: {
                        backgroundColor: COLORS.primary_back
                    },
                    scrollEnabled: false,
                    
                    tabBarLabelStyle: {
                        color: COLORS.white,
                        fontSize:8,
                        fontWeight:'bold',
                        textTransform:'capitalize'
                    },
                    tabBarIndicatorStyle: {
                        backgroundColor: COLORS.primary,
                    },
                    tabBarIconStyle: {
                        marginHorizontal: 20,
                    },
                    tabBarIndicatorStyle: {
                        backgroundColor: COLORS.primary,
                        width: 40,
                        marginHorizontal: 20,
                    },

                }}
            >
                <Tab.Screen name="Overview" children={() => <Overview />} />
                <Tab.Screen name="Spot" children={() => <Overview />} />
                <Tab.Screen name="Funding" children={() => <Overview />} />
                <Tab.Screen name="Margin" children={() => <Overview />} />
                <Tab.Screen name="Futures" children={() => <Overview />} />
                <Tab.Screen name="Earn" children={() => <Overview />} />

            </Tab.Navigator>
        </View>
    )
}

export default Tabs

const styles = StyleSheet.create({})