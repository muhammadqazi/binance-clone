import Forgot_Password from "./Auth/Forgot_Password/Forgot_Password";
import New_device from "./Auth/New_device/New_device";
import Sign_In from "./Auth/Sign_In/Sign_In";
import Signup_form from "./Auth/Sign_up/Signup_form";
import Sign_up from "./Auth/Sign_up/Sign_up";
import UserPrompt from "./Walkthrough/UserPrompt";
import Walkthrough from "./Walkthrough/Walkthrough";
import Funding from "./Wallets/Tabs/Funding/Funding";
import Futures from "./Wallets/Tabs/Futures/Futures";
import Margin from "./Wallets/Tabs/Margin/Margin";
import Overview from "./Wallets/Tabs/Overview/Overview";
import Spot from "./Wallets/Tabs/Spot/Spot";

export {
    Walkthrough,
    Sign_In,
    Sign_up,
    Signup_form,
    Forgot_Password,
    New_device,
    Overview,
    Spot,
    Funding,
    UserPrompt,
    Margin,
    Futures
}