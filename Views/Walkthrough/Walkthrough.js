import { StyleSheet, Text, View, Image, TouchableOpacity } from 'react-native';
import React, { useState } from 'react';
import { animations, COLORS, icons, SIZES } from '../../constants';
import LottieView from 'lottie-react-native';
import DropDownPicker from "react-native-dropdown-picker";


const Walkthrough = () => {
    const [open2, setOpen2] = useState(false);
    const [value2, setValue2] = useState(null);
    const [items, setItems] = useState([
        { label: 'Qatar', value: '1' },
        { label: 'America', value: '2' }
    ]);


    function render_dropdown() {
        return (
            <View style={[styles.viewContainer, Platform.OS === 'android' && open2 && styles.androidContainer, { zIndex: 4000 }]}>
                <DropDownPicker

                    dropDownContainerStyle={{
                        backgroundColor: COLORS.transparentBlack,
                        borderWidth: 1,
                        borderLeftColor: COLORS.darkGray,
                        borderRightColor: COLORS.darkGray,
                        borderBottomColor: COLORS.darkGray,

                    }}
                    ArrowDownIconComponent={({ style }) =>
                        <Image style={{ height: 20, width: 20, marginRight: 10 }} source={icons.arrows} />
                    }

                    listMode={'SCROLLVIEW'}
                    tickIconStyle={{ tintColor: COLORS.primary }}
                    arrowIconStyle={{ tintColor: COLORS.primary }}

                    placeholderStyle={{
                        color: COLORS.white,
                    }}
                    listItemLabelStyle={{
                        color: 'white'
                    }}
                    style={{ borderColor: COLORS.darkGray, backgroundColor: COLORS.transparentBlack }}
                    labelStyle={{
                        fontWeight: "bold",
                        color: 'white'
                    }}
                    placeholder={"Select Country"}
                    open={open2}
                    value={value2}
                    items={items}
                    setOpen={setOpen2}
                    setValue={setValue2}
                    onChangeValue={(value) => {
                        console.log(value);
                    }}
                />
            </View>
        )
    }

    function render_animations() {
        return (
            <View style={{
                marginTop: SIZES.padding * 4,
                alignItems: 'center',
            }}>
                <LottieView style={{ height: 150, width: 150 }} source={animations.globe} autoPlay loop={true} />
                <View>
                    <Text style={{
                        fontWeight: 'bold',
                        color: COLORS.white,
                        letterSpacing: 1,
                        fontSize: 22,
                    }}>Before we start, we'll have to</Text>
                    <Text style={{
                        fontWeight: 'bold',
                        color: COLORS.white,
                        letterSpacing: 1,
                        fontSize: 22,
                        textAlign: 'center'
                    }}>know your current location</Text>
                    <Text style={{
                        fontWeight: 'bold',
                        color: COLORS.white,
                        letterSpacing: 1,
                        fontSize: 22,
                        textAlign: 'center'
                    }}>residence</Text>
                </View>

                {render_dropdown()}

                <View style={{
                    marginHorizontal: '6%',
                    marginTop: 10
                }}>
                    <Text style={{
                        color: COLORS.darkGray,
                        fontSize: 12,
                    }}>The registration process is subjected to change based on the information you provide</Text>
                </View>
            </View>
        )
    }

    function render_footer() {
        return (
            <View style={{
                alignItems: 'flex-end',
                marginBottom: 20,
                marginRight: 20
            }}>
                <TouchableOpacity style={{
                    backgroundColor: COLORS.primary,
                    height: 45,
                    width: 45,
                    borderRadius: 5,
                    alignItems: "center",
                    justifyContent: "center",
                }}>
                    <Image style={{ height: 30, width: 30 }} source={icons.arrow_right} />
                </TouchableOpacity>
            </View>
        )
    }

    return (
        <View style={{
            flex: 1,
            backgroundColor: COLORS.primary_back,
            justifyContent: "space-between"

        }}>

            {/* Animations and dropdown */}
            {render_animations()}


            {/* Footer */}
            {render_footer()}

        </View>
    );
};

const styles = StyleSheet.create({
    viewContainer: { marginHorizontal: '7%', marginTop: 50 },
    androidContainer: {
        minHeight: 500,
        marginBottom: 428,
    },
});

export default Walkthrough;
