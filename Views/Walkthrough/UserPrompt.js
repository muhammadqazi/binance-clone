import { View, Text, Image, TouchableOpacity } from 'react-native'
import React from 'react'
import { COLORS, icons } from '../../constants'
import { TextButton } from '../../components'


const Guide = ({ icon, label, description }) => {
    return (
        <View style={{
            marginTop: 35,
            flexDirection: 'row',
        }}>
            <Image source={icon} style={{
                height: 25,
                width: 25,
                tintColor: COLORS.primary,
            }} />


            <View style={{
                marginLeft: 15,
            }}>
                <Text style={{
                    color: COLORS.white,
                    fontWeight: 'bold',
                    fontSize: 16,
                }}>{label}</Text>
                <Text style={{
                    color: COLORS.darkGray,
                    marginTop: 5,
                    fontSize: 12
                }}>{description}</Text>
            </View>
        </View>
    )
}
const UserPrompt = () => {

    function renderFooter() {
        return (
            <View style={{
                marginTop: 30,
            }}>
                <Text style={{
                    color: COLORS.darkGray,
                }}>
                    By creating an account, you agree to our .
                    <Text onPress={() => alert("Terms")} style={{
                        color: COLORS.primary,
                    }}>
                        Terms and Conditions
                    </Text> and .

                    <Text onPress={() => alert("Privacy")} style={{
                        color: COLORS.primary,
                    }}>
                        Data Protection Guidelines.
                    </Text>

                </Text>

                {/* Buttons */}

                <View style={{
                    marginTop: 30,
                }}>
                    <TextButton
                        label={"Create Personal Account"}
                    />


                    {/* Or, */}
                    <View style={{ flexDirection: 'row', alignItems: 'center', marginTop: 20 }}>
                        <View style={{ flex: 1, height: 1, backgroundColor: COLORS.darkGray }} />
                        <View style={{
                            marginHorizontal: 10,

                        }}>
                            <Text style={{ width: 100, color: COLORS.darkGray, }}>or, for business</Text>
                        </View>
                        <View style={{ flex: 1, height: 1, backgroundColor: COLORS.darkGray }} />
                    </View>

                    <TextButton containerStyle={{
                        marginTop: 20,
                        backgroundColor: COLORS.darkGray,
                    }}
                        label={"Create Entity Account"}
                        labelStyle={{ color: COLORS.white }}
                    />

                </View>


                <View style={{
                    marginTop: 30,
                }}>
                    <Text style={{
                        color: COLORS.darkGray,
                        fontWeight: 'bold',
                    }}>Already Register? .
                        <Text onPress={() => alert("Login")} style={{
                            color: COLORS.brown,
                            fontWeight: 'bold',
                        }}>
                            Login
                        </Text>
                    </Text>
                </View>
            </View>
        )
    }
    return (
        <View style={{
            flex: 1,
            backgroundColor: COLORS.primary_back,
        }}>

            {/* Header */}
            <View style={{
                marginHorizontal: 20,
                marginTop: 50
            }}>
                <Text style={{
                    fontWeight: 'bold',
                    color: COLORS.white,
                    letterSpacing: 1,
                    fontSize: 22,

                }}>Create Your Account</Text>


                <Text style={{
                    color: COLORS.darkGray,
                    marginTop: 20,
                }}>
                    Binance is the world's largest crypto exchange platform.
                </Text>

                {/* Options */}
                <Guide icon={icons.account} label={"Create Account"} description={"Enter your account details."} />
                <Guide icon={icons.id} label={"Verify Identity"} description={"Verify your identity to protect your account."} />
                <Guide icon={icons.diamond} label={"Unlock Prize"} description={"Get your prize and start trading."} />


                {/* Terms and Buttons */}
                {renderFooter()}


            </View>

        </View>
    )
}

export default UserPrompt