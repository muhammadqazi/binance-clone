# Binance Clone
Binance cryptocurrency exchange, this repository contains the code for Binance mobile application clone which is build in react-native.



## Setup

Clone the project

```
git clone https://github.com/muhammadqazi/Binance-Clone.git
```

Install the dependencies

```
npm install
```
Or
```
yarn
```
Start the metro 

```
npx react-native start
```
Run on android or IOS, for android it needs an android studio setup for IOS it needs an XCode setup

For iOS

Install dependencies

```
cd ios
pod install
```

```
npx react-native run-ios
```

For Android 

```
npx react-native run-android
```

