import animations from "./animations";
import icons from "./icons";
import theme, { COLORS, SIZES, FONTS } from "./theme";

export {
    theme,
    COLORS,
    SIZES,
    FONTS,
    animations,
    icons
}
