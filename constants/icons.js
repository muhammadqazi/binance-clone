const arrows = require("../assets/images/icons8-direction-96.png");
const arrow_right = require("../assets/images/icons8-right-100.png");
const cross = require("../assets/images/icons8-macos-close-90.png");
const eye = require("../assets/images/eye.png");
const eye_off = require("../assets/images/hidden.png");
const forward = require("../assets/images/icons8-forward-100.png");
const up = require("../assets/images/icons8-sort-up-90.png");
const tick = require("../assets/images/icons8-ok-120.png");

const spot = require("../assets/images/spot.png");
const fund = require("../assets/images/fund.png");
const margin = require("../assets/images/margin.png");
const up_margin = require("../assets/images/up.png");
const usd_note = require("../assets/images/usdNote.png");
const coin = require("../assets/images/coin.png");
const pig = require("../assets/images/pig.png");
const dig = require("../assets/images/dig.png");
const wallet = require("../assets/images/wallet.png");
const information = require("../assets/images/icons8-information-60.png");
const search = require("../assets/images/search.png");
const aave = require("../assets/images/Aave-Crypto-Logo.png");
const creditcard = require("../assets/images/creditcard.png");
const p2p = require("../assets/images/p2p.png");
const gift = require("../assets/images/gift.png");
const account = require("../assets/images/icons8-account-100.png");
const diamond = require("../assets/images/icons8-diamond-64.png");
const id = require("../assets/images/icons8-us-news-100.png");
const filter = require("../assets/images/filter.png");
const flower = require("../assets/images/icons8-pollen-160.png");
const timer = require("../assets/images/icons8-timer-100.png");



export default {
    arrows,
    arrow_right,
    cross,
    eye,
    eye_off,
    forward,
    up,
    tick,
    spot,
    fund,
    margin,
    up_margin,
    usd_note,
    coin,
    pig,
    dig,
    wallet,
    information,
    search,
    aave,
    creditcard,
    p2p,
    gift,
    account,
    diamond,
    id,
    filter,
    flower,
    timer,
}
