import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from '@react-navigation/stack';

import React from "react";
import { COLORS } from "../constants";
import {
    Forgot_Password,
    Funding,
    Futures,
    Margin,
    New_device,
    Overview,
    Signup_form,
    Sign_In,
    Sign_up,
    Spot,
    UserPrompt,
    Walkthrough
} from "../Views";


const Stack = createStackNavigator();

const RootStack = () => {
    return (
        <NavigationContainer>
            <Stack.Navigator
                screenOptions={{
                    headerStyle: {
                        backgroundColor: "transparent"
                    },
                    headerStyle: { elevation: 0 },
                    cardStyle: { backgroundColor: '#fff' },
                    animationEnabled: true,
                    headerTintColor: COLORS.primary,
                    headerTransparent: true,
                    headerTitle: '',
                    headerLeft: null,
                    headerLeftContainerStyle: {
                        paddingLeft: 20
                    }

                }}

                initialRouteName="futures"
            >

                {/* Wallets */}

                <Stack.Screen name="overview" component={Overview} />
                <Stack.Screen name="spot" component={Spot} />
                <Stack.Screen name="funding" component={Funding} />
                <Stack.Screen name="margin" component={Margin} />
                <Stack.Screen name="futures" component={Futures} />


                {/* Authentication */}

                <Stack.Screen name="newdevice" component={New_device} />
                <Stack.Screen name="walkthrough" component={Walkthrough} />
                <Stack.Screen name="userPromt" component={UserPrompt} />
                <Stack.Screen name="signin" component={Sign_In} />
                <Stack.Screen name="signup" component={Sign_up} />
                <Stack.Screen name="signupForm" component={Signup_form} />
                <Stack.Screen name="forgotPassword" component={Forgot_Password} />



            </Stack.Navigator>
        </NavigationContainer>
    );
}

export default RootStack;